package org.codechimp.testapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestJavaSpringAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestJavaSpringAppApplication.class, args);
	}

}
